namespace CinemaStudio.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Фильм
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID_Фильма { get; set; }

        [Required]
        [StringLength(50)]
        public string Название { get; set; }

        [DataType(DataType.Date)]
        [Column("Дата релиза")]
        public DateTime Дата_релиза { get; set; }

        [StringLength(50)]
        public string БюджетФильма { get; set; }

        [DataType(DataType.Time)]
        public DateTime Длительность { get; set; }

        [Required]
        [StringLength(50)]
        public string Язык { get; set; }

        [Required]
        [StringLength(50)]
        public string Жанр { get; set; }

        public double? Рейтинг { get; set; }

        public int? ВозрастноеОграничение { get; set; }

        [StringLength(50)]
        public string Статус { get; set; }

        public int ID_Предпроизводства { get; set; }

        public int ID_СъемочногоПерида { get; set; }

        public int ID_Постпроизводства { get; set; }

        public int ID_ТИС { get; set; }

        public int ID_Тиражирования { get; set; }

        public virtual Постпроизводство Постпроизводство { get; set; }

        public virtual Предпроизводство Предпроизводство { get; set; }

        public virtual Съёмочный_период Съёмочный_период { get; set; }

        public virtual Тиражирование Тиражирование { get; set; }

        public virtual Титры_и_субтитры Титры_и_субтитры { get; set; }
    }
}
