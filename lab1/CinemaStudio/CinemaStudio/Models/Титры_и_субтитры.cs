namespace CinemaStudio.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Титры и субтитры")]
    public partial class Титры_и_субтитры
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Титры_и_субтитры()
        {
            Фильм = new HashSet<Фильм>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID_ТИС { get; set; }

        [Column("Язык текста")]
        [StringLength(50)]
        public string Язык_текста { get; set; }

        [DataType(DataType.Time)]
        [Column("Продолжительность титров")]
        public DateTime? Продолжительность_титров { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Фильм> Фильм { get; set; }
    }
}
