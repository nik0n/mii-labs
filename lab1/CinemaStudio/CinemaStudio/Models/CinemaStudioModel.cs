﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;

namespace CinemaStudio.Models
{
    public partial class CinemaStudioModel : DbContext
    {
        public CinemaStudioModel()
            : base("name=CinemaStudioModel")
        {
        }

        public virtual DbSet<Постпроизводство> Постпроизводство { get; set; }
        public virtual DbSet<Предпроизводство> Предпроизводство { get; set; }
        public virtual DbSet<Сотрудник> Сотрудник { get; set; }
        public virtual DbSet<Съёмочный_период> Съёмочный_период { get; set; }
        public virtual DbSet<Тиражирование> Тиражирование { get; set; }
        public virtual DbSet<Титры_и_субтитры> Титры_и_субтитры { get; set; }
        public virtual DbSet<Фильм> Фильм { get; set; }
        public virtual DbSet<Человек> Человек { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Постпроизводство>()
                .HasMany(e => e.Фильм)
                .WithRequired(e => e.Постпроизводство)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Предпроизводство>()
                .HasMany(e => e.Фильм)
                .WithRequired(e => e.Предпроизводство)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Сотрудник>()
                .HasMany(e => e.Постпроизводство)
                .WithRequired(e => e.Сотрудник)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Сотрудник>()
                .HasMany(e => e.Предпроизводство)
                .WithRequired(e => e.Сотрудник)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Сотрудник>()
                .HasMany(e => e.Съёмочный_период)
                .WithRequired(e => e.Сотрудник)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Съёмочный_период>()
                .HasMany(e => e.Фильм)
                .WithRequired(e => e.Съёмочный_период)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Тиражирование>()
                .HasMany(e => e.Фильм)
                .WithRequired(e => e.Тиражирование)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Титры_и_субтитры>()
                .HasMany(e => e.Фильм)
                .WithRequired(e => e.Титры_и_субтитры)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Человек>()
                .HasMany(e => e.Сотрудник)
                .WithRequired(e => e.Человек)
                .WillCascadeOnDelete(false);
        }
    }
}
