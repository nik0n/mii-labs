namespace CinemaStudio.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Съёмочный период")]
    public partial class Съёмочный_период
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Съёмочный_период()
        {
            Фильм = new HashSet<Фильм>();
        }


        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID_СъемочногоПерида { get; set; }

        [DataType(DataType.Time)]
        [Column("Количество отснятого материала")]
        public DateTime? Количество_отснятого_материала { get; set; }

        [DataType(DataType.Time)]
        [Column("Количество записанных аудио")]
        public DateTime? Количество_записанных_аудио { get; set; }

        public int ID_Сотрудника { get; set; }

        public virtual Сотрудник Сотрудник { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Фильм> Фильм { get; set; }
    }
}
