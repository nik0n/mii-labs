namespace CinemaStudio.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Человек
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Человек()
        {
            Сотрудник = new HashSet<Сотрудник>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID_Человека { get; set; }

        [Required]
        [StringLength(50)]
        public string Имя { get; set; }

        [Required]
        [StringLength(50)]
        public string Фамилия { get; set; }

        [StringLength(50)]
        public string Отчество { get; set; }

        public bool? Пол { get; set; }

        [StringLength(50)]
        public string Образование { get; set; }

        [StringLength(50)]
        public string Телефон { get; set; }

        [DataType(DataType.Date)]
        public DateTime ДатаРождения { get; set; }

        [Required]
        [StringLength(50)]
        public string СерияНомерПаспорта { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Сотрудник> Сотрудник { get; set; }
    }
}
