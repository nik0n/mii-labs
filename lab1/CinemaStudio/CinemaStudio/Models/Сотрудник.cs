namespace CinemaStudio.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Сотрудник
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Сотрудник()
        {
            Постпроизводство = new HashSet<Постпроизводство>();
            Предпроизводство = new HashSet<Предпроизводство>();
            Съёмочный_период = new HashSet<Съёмочный_период>();
        }

        [StringLength(50)]
        public string Должность { get; set; }

        public int? ЗП { get; set; }

        public int? СтажРаботы { get; set; }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID_Сотрудника { get; set; }

        public int ID_Человека { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Постпроизводство> Постпроизводство { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Предпроизводство> Предпроизводство { get; set; }

        public virtual Человек Человек { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Съёмочный_период> Съёмочный_период { get; set; }
    }
}
