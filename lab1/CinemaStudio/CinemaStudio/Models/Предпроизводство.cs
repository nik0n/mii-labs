namespace CinemaStudio.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Предпроизводство
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Предпроизводство()
        {
            Фильм = new HashSet<Фильм>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID_Предпроизводства { get; set; }

        [StringLength(50)]
        public string Тема { get; set; }

        [StringLength(50)]
        public string Сюжет { get; set; }

        [StringLength(50)]
        public string Проблематика { get; set; }

        public int ID_Сотрудника { get; set; }

        public virtual Сотрудник Сотрудник { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Фильм> Фильм { get; set; }
    }
}
