namespace CinemaStudio.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Постпроизводство
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Постпроизводство()
        {
            Фильм = new HashSet<Фильм>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID_Постпроизводства { get; set; }

        [DataType(DataType.Time)]
        [Column("Время смонтированного фильма")]
        public DateTime? Время_смонтированного_фильма { get; set; }

        [Column("Тип фонограммы")]
        [StringLength(50)]
        public string Тип_фонограммы { get; set; }

        [Column("Тип шумов")]
        [StringLength(50)]
        public string Тип_шумов { get; set; }

        public int ID_Сотрудника { get; set; }

        public virtual Сотрудник Сотрудник { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Фильм> Фильм { get; set; }
    }
}
