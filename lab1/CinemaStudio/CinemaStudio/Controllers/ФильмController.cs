﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CinemaStudio.Models;

namespace CinemaStudio.Controllers
{
    public class ФильмController : Controller
    {
        private CinemaStudioModel db = new CinemaStudioModel();

        // GET: Фильм
        public ActionResult Index()
        {
            var фильм = db.Фильм.Include(ф => ф.Постпроизводство).Include(ф => ф.Предпроизводство).Include(ф => ф.Съёмочный_период).Include(ф => ф.Тиражирование).Include(ф => ф.Титры_и_субтитры);
            return View(фильм.ToList());
        }

        [HttpPost]
        public ActionResult Index(string genre, int? ageRestriction)
        {
            if (genre == "" || ageRestriction == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var Фильм = db.Фильм
                .Where(a => a.Жанр == genre)
                .Where(a => a.ВозрастноеОграничение == ageRestriction);
            return View(Фильм.ToList());
        }

        // GET: Фильм/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Фильм фильм = db.Фильм.Find(id);
            if (фильм == null)
            {
                return HttpNotFound();
            }
            return View(фильм);
        }

        // GET: Фильм/Create
        public ActionResult Create()
        {
            ViewBag.ID_Постпроизводства = new SelectList(db.Постпроизводство, "ID_Постпроизводства", "Тип_фонограммы");
            ViewBag.ID_Предпроизводства = new SelectList(db.Предпроизводство, "ID_Предпроизводства", "Тема");
            ViewBag.ID_СъемочногоПерида = new SelectList(db.Съёмочный_период, "ID_СъемочногоПерида", "ID_СъемочногоПерида");
            ViewBag.ID_Тиражирования = new SelectList(db.Тиражирование, "ID_Тиражирования", "ID_Тиражирования");
            ViewBag.ID_ТИС = new SelectList(db.Титры_и_субтитры, "ID_ТИС", "Язык_текста");
            return View();
        }

        // POST: Фильм/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. 
        // Дополнительные сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID_Фильма,Название,Дата_релиза,БюджетФильма,Длительность,Язык,Жанр,Рейтинг,ВозрастноеОграничение,Статус,ID_Предпроизводства,ID_СъемочногоПерида,ID_Постпроизводства,ID_ТИС,ID_Тиражирования")] Фильм фильм)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Фильм.Add(фильм);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }

                ViewBag.ID_Постпроизводства = new SelectList(db.Постпроизводство, "ID_Постпроизводства", "Тип_фонограммы", фильм.ID_Постпроизводства);
                ViewBag.ID_Предпроизводства = new SelectList(db.Предпроизводство, "ID_Предпроизводства", "Тема", фильм.ID_Предпроизводства);
                ViewBag.ID_СъемочногоПерида = new SelectList(db.Съёмочный_период, "ID_СъемочногоПерида", "ID_СъемочногоПерида", фильм.ID_СъемочногоПерида);
                ViewBag.ID_Тиражирования = new SelectList(db.Тиражирование, "ID_Тиражирования", "ID_Тиражирования", фильм.ID_Тиражирования);
                ViewBag.ID_ТИС = new SelectList(db.Титры_и_субтитры, "ID_ТИС", "Язык_текста", фильм.ID_ТИС);
                return View(фильм);
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

        }

        // GET: Фильм/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Фильм фильм = db.Фильм.Find(id);
            if (фильм == null)
            {
                return HttpNotFound();
            }
            ViewBag.ID_Постпроизводства = new SelectList(db.Постпроизводство, "ID_Постпроизводства", "Тип_фонограммы", фильм.ID_Постпроизводства);
            ViewBag.ID_Предпроизводства = new SelectList(db.Предпроизводство, "ID_Предпроизводства", "Тема", фильм.ID_Предпроизводства);
            ViewBag.ID_СъемочногоПерида = new SelectList(db.Съёмочный_период, "ID_СъемочногоПерида", "ID_СъемочногоПерида", фильм.ID_СъемочногоПерида);
            ViewBag.ID_Тиражирования = new SelectList(db.Тиражирование, "ID_Тиражирования", "ID_Тиражирования", фильм.ID_Тиражирования);
            ViewBag.ID_ТИС = new SelectList(db.Титры_и_субтитры, "ID_ТИС", "Язык_текста", фильм.ID_ТИС);
            return View(фильм);
        }

        // POST: Фильм/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. 
        // Дополнительные сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_Фильма,Название,Дата_релиза,БюджетФильма,Длительность,Язык,Жанр,Рейтинг,ВозрастноеОграничение,Статус,ID_Предпроизводства,ID_СъемочногоПерида,ID_Постпроизводства,ID_ТИС,ID_Тиражирования")] Фильм фильм)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Entry(фильм).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                ViewBag.ID_Постпроизводства = new SelectList(db.Постпроизводство, "ID_Постпроизводства", "Тип_фонограммы", фильм.ID_Постпроизводства);
                ViewBag.ID_Предпроизводства = new SelectList(db.Предпроизводство, "ID_Предпроизводства", "Тема", фильм.ID_Предпроизводства);
                ViewBag.ID_СъемочногоПерида = new SelectList(db.Съёмочный_период, "ID_СъемочногоПерида", "ID_СъемочногоПерида", фильм.ID_СъемочногоПерида);
                ViewBag.ID_Тиражирования = new SelectList(db.Тиражирование, "ID_Тиражирования", "ID_Тиражирования", фильм.ID_Тиражирования);
                ViewBag.ID_ТИС = new SelectList(db.Титры_и_субтитры, "ID_ТИС", "Язык_текста", фильм.ID_ТИС);
                return View(фильм);
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

        }

        // GET: Фильм/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Фильм фильм = db.Фильм.Find(id);
            if (фильм == null)
            {
                return HttpNotFound();
            }
            return View(фильм);
        }

        // POST: Фильм/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                Фильм фильм = db.Фильм.Find(id);
                db.Фильм.Remove(фильм);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
