﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CinemaStudio.Models;

namespace CinemaStudio.Controllers
{
    public class ПредпроизводствоController : Controller
    {
        private CinemaStudioModel db = new CinemaStudioModel();

        // GET: Предпроизводство
        public ActionResult Index()
        {
            var предпроизводство = db.Предпроизводство.Include(п => п.Сотрудник);
            return View(предпроизводство.ToList());
        }

        // GET: Предпроизводство/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Предпроизводство предпроизводство = db.Предпроизводство.Find(id);
            if (предпроизводство == null)
            {
                return HttpNotFound();
            }
            return View(предпроизводство);
        }

        // GET: Предпроизводство/Create
        public ActionResult Create()
        {
            ViewBag.ID_Сотрудника = new SelectList(db.Сотрудник, "ID_Сотрудника", "Должность");
            return View();
        }

        // POST: Предпроизводство/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. 
        // Дополнительные сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID_Предпроизводства,Тема,Сюжет,Проблематика,ID_Сотрудника")] Предпроизводство предпроизводство)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Предпроизводство.Add(предпроизводство);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }

                ViewBag.ID_Сотрудника = new SelectList(db.Сотрудник, "ID_Сотрудника", "Должность", предпроизводство.ID_Сотрудника);
                return View(предпроизводство);
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

        }

        // GET: Предпроизводство/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Предпроизводство предпроизводство = db.Предпроизводство.Find(id);
            if (предпроизводство == null)
            {
                return HttpNotFound();
            }
            ViewBag.ID_Сотрудника = new SelectList(db.Сотрудник, "ID_Сотрудника", "Должность", предпроизводство.ID_Сотрудника);
            return View(предпроизводство);
        }

        // POST: Предпроизводство/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. 
        // Дополнительные сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_Предпроизводства,Тема,Сюжет,Проблематика,ID_Сотрудника")] Предпроизводство предпроизводство)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Entry(предпроизводство).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                ViewBag.ID_Сотрудника = new SelectList(db.Сотрудник, "ID_Сотрудника", "Должность", предпроизводство.ID_Сотрудника);
                return View(предпроизводство);
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

        }

        // GET: Предпроизводство/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Предпроизводство предпроизводство = db.Предпроизводство.Find(id);
            if (предпроизводство == null)
            {
                return HttpNotFound();
            }
            return View(предпроизводство);
        }

        // POST: Предпроизводство/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                Предпроизводство предпроизводство = db.Предпроизводство.Find(id);
                db.Предпроизводство.Remove(предпроизводство);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
