﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CinemaStudio.Models;

namespace CinemaStudio.Controllers
{
    public class ТиражированиеController : Controller
    {
        private CinemaStudioModel db = new CinemaStudioModel();

        // GET: Тиражирование
        public ActionResult Index()
        {
            return View(db.Тиражирование.ToList());
        }

        // GET: Тиражирование/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Тиражирование тиражирование = db.Тиражирование.Find(id);
            if (тиражирование == null)
            {
                return HttpNotFound();
            }
            return View(тиражирование);
        }

        // GET: Тиражирование/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Тиражирование/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. 
        // Дополнительные сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID_Тиражирования,Количество_копий,Количество_рекламных_договоров")] Тиражирование тиражирование)
        {
            if (ModelState.IsValid)
            {
                db.Тиражирование.Add(тиражирование);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(тиражирование);
        }

        // GET: Тиражирование/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Тиражирование тиражирование = db.Тиражирование.Find(id);
            if (тиражирование == null)
            {
                return HttpNotFound();
            }
            return View(тиражирование);
        }

        // POST: Тиражирование/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. 
        // Дополнительные сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_Тиражирования,Количество_копий,Количество_рекламных_договоров")] Тиражирование тиражирование)
        {
            if (ModelState.IsValid)
            {
                db.Entry(тиражирование).State = EntityState.Modified;

                SqlParameter tid = new SqlParameter("@TID", тиражирование.ID_Тиражирования);
                var sqlres = db.Database.SqlQuery<int>("EXEC dbo.changeStatus @TID", tid);

               // var sqlresult = db.Database.ExecuteSqlCommand(String.Format("exec dbo.[changeStatus] {0}, {1}", ));

                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(тиражирование);
        }

        // GET: Тиражирование/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Тиражирование тиражирование = db.Тиражирование.Find(id);
            if (тиражирование == null)
            {
                return HttpNotFound();
            }
            return View(тиражирование);
        }

        // POST: Тиражирование/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Тиражирование тиражирование = db.Тиражирование.Find(id);
            db.Тиражирование.Remove(тиражирование);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
