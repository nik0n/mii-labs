﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CinemaStudio.Models;

namespace CinemaStudio.Controllers
{
    public class Съёмочный_периодController : Controller
    {
        private CinemaStudioModel db = new CinemaStudioModel();

        // GET: Съёмочный_период
        public ActionResult Index()
        {
            var съёмочный_период = db.Съёмочный_период.Include(с => с.Сотрудник);
            return View(съёмочный_период.ToList());
        }

        // GET: Съёмочный_период/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Съёмочный_период съёмочный_период = db.Съёмочный_период.Find(id);
            if (съёмочный_период == null)
            {
                return HttpNotFound();
            }
            return View(съёмочный_период);
        }

        // GET: Съёмочный_период/Create
        public ActionResult Create()
        {
            ViewBag.ID_Сотрудника = new SelectList(db.Сотрудник, "ID_Сотрудника", "Должность");
            return View();
        }

        // POST: Съёмочный_период/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. 
        // Дополнительные сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID_СъемочногоПерида,Количество_отснятого_материала,Количество_записанных_аудио,ID_Сотрудника")] Съёмочный_период съёмочный_период)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Съёмочный_период.Add(съёмочный_период);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }

                ViewBag.ID_Сотрудника = new SelectList(db.Сотрудник, "ID_Сотрудника", "Должность", съёмочный_период.ID_Сотрудника);
                return View(съёмочный_период);
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

        }

        // GET: Съёмочный_период/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Съёмочный_период съёмочный_период = db.Съёмочный_период.Find(id);
            if (съёмочный_период == null)
            {
                return HttpNotFound();
            }
            ViewBag.ID_Сотрудника = new SelectList(db.Сотрудник, "ID_Сотрудника", "Должность", съёмочный_период.ID_Сотрудника);
            return View(съёмочный_период);
        }

        // POST: Съёмочный_период/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. 
        // Дополнительные сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_СъемочногоПерида,Количество_отснятого_материала,Количество_записанных_аудио,ID_Сотрудника")] Съёмочный_период съёмочный_период)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Entry(съёмочный_период).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                ViewBag.ID_Сотрудника = new SelectList(db.Сотрудник, "ID_Сотрудника", "Должность", съёмочный_период.ID_Сотрудника);
                return View(съёмочный_период);
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

        }

        // GET: Съёмочный_период/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Съёмочный_период съёмочный_период = db.Съёмочный_период.Find(id);
            if (съёмочный_период == null)
            {
                return HttpNotFound();
            }
            return View(съёмочный_период);
        }

        // POST: Съёмочный_период/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                Съёмочный_период съёмочный_период = db.Съёмочный_период.Find(id);
                db.Съёмочный_период.Remove(съёмочный_период);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
