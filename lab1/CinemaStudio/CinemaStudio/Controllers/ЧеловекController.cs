﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CinemaStudio.Models;

namespace CinemaStudio.Controllers
{
    public class ЧеловекController : Controller
    {
        private CinemaStudioModel db = new CinemaStudioModel();

        // GET: Человек
        public ActionResult Index()
        {
            return View(db.Человек.ToList());
        }

        [HttpPost]
        public ActionResult Index(string lastName)
        {
            if (lastName == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            List<Человек> people = new List<Человек>();
            people.Add(db.Человек.Where(a => a.Фамилия == lastName).FirstOrDefault());
            foreach (Человек item in db.Человек.Where(a => a.Фамилия != lastName))
            {
                people.Add(item);
            }

            return View(people);
        }

        // GET: Человек/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Человек человек = db.Человек.Find(id);
            if (человек == null)
            {
                return HttpNotFound();
            }
            return View(человек);
        }

        // GET: Человек/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Человек/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. 
        // Дополнительные сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID_Человека,Имя,Фамилия,Отчество,Пол,Образование,Телефон,ДатаРождения,СерияНомерПаспорта")] Человек человек)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Человек.Add(человек);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }

                return View(человек);
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

        }

        // GET: Человек/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Человек человек = db.Человек.Find(id);
            if (человек == null)
            {
                return HttpNotFound();
            }
            return View(человек);
        }

        // POST: Человек/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. 
        // Дополнительные сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_Человека,Имя,Фамилия,Отчество,Пол,Образование,Телефон,ДатаРождения,СерияНомерПаспорта")] Человек человек)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Entry(человек).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                return View(человек);
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

        }

        // GET: Человек/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Человек человек = db.Человек.Find(id);
            if (человек == null)
            {
                return HttpNotFound();
            }
            return View(человек);
        }

        // POST: Человек/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                Человек человек = db.Человек.Find(id);
                db.Человек.Remove(человек);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
