﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CinemaStudio.Models;

namespace CinemaStudio.Controllers
{
    public class ПостпроизводствоController : Controller
    {
        private CinemaStudioModel db = new CinemaStudioModel();

        // GET: Постпроизводство
        public ActionResult Index()
        {
            var постпроизводство = db.Постпроизводство.Include(п => п.Сотрудник);
            return View(постпроизводство.ToList());
        }

        // GET: Постпроизводство/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Постпроизводство постпроизводство = db.Постпроизводство.Find(id);
            if (постпроизводство == null)
            {
                return HttpNotFound();
            }
            return View(постпроизводство);
        }

        // GET: Постпроизводство/Create
        public ActionResult Create()
        {
            ViewBag.ID_Сотрудника = new SelectList(db.Сотрудник, "ID_Сотрудника", "Должность");
            return View();
        }

        // POST: Постпроизводство/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. 
        // Дополнительные сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID_Постпроизводства,Время_смонтированного_фильма,Тип_фонограммы,Тип_шумов,ID_Сотрудника")] Постпроизводство постпроизводство)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Постпроизводство.Add(постпроизводство);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }

                ViewBag.ID_Сотрудника = new SelectList(db.Сотрудник, "ID_Сотрудника", "Должность", постпроизводство.ID_Сотрудника);
                return View(постпроизводство);
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

        }

        // GET: Постпроизводство/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Постпроизводство постпроизводство = db.Постпроизводство.Find(id);
            if (постпроизводство == null)
            {
                return HttpNotFound();
            }
            ViewBag.ID_Сотрудника = new SelectList(db.Сотрудник, "ID_Сотрудника", "Должность", постпроизводство.ID_Сотрудника);
            return View(постпроизводство);
        }

        // POST: Постпроизводство/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. 
        // Дополнительные сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_Постпроизводства,Время_смонтированного_фильма,Тип_фонограммы,Тип_шумов,ID_Сотрудника")] Постпроизводство постпроизводство)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Entry(постпроизводство).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                ViewBag.ID_Сотрудника = new SelectList(db.Сотрудник, "ID_Сотрудника", "Должность", постпроизводство.ID_Сотрудника);
                return View(постпроизводство);
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

        }

        // GET: Постпроизводство/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Постпроизводство постпроизводство = db.Постпроизводство.Find(id);
            if (постпроизводство == null)
            {
                return HttpNotFound();
            }
            return View(постпроизводство);
        }

        // POST: Постпроизводство/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                Постпроизводство постпроизводство = db.Постпроизводство.Find(id);
                db.Постпроизводство.Remove(постпроизводство);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
