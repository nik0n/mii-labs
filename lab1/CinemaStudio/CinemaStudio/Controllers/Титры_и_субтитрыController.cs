﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CinemaStudio.Models;

namespace CinemaStudio.Controllers
{
    public class Титры_и_субтитрыController : Controller
    {
        private CinemaStudioModel db = new CinemaStudioModel();

        // GET: Титры_и_субтитры
        public ActionResult Index()
        {
            return View(db.Титры_и_субтитры.ToList());
        }

        // GET: Титры_и_субтитры/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Титры_и_субтитры титры_и_субтитры = db.Титры_и_субтитры.Find(id);
            if (титры_и_субтитры == null)
            {
                return HttpNotFound();
            }
            return View(титры_и_субтитры);
        }

        // GET: Титры_и_субтитры/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Титры_и_субтитры/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. 
        // Дополнительные сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID_ТИС,Язык_текста,Продолжительность_титров")] Титры_и_субтитры титры_и_субтитры)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Титры_и_субтитры.Add(титры_и_субтитры);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }

                return View(титры_и_субтитры);
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

        }

        // GET: Титры_и_субтитры/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Титры_и_субтитры титры_и_субтитры = db.Титры_и_субтитры.Find(id);
            if (титры_и_субтитры == null)
            {
                return HttpNotFound();
            }
            return View(титры_и_субтитры);
        }

        // POST: Титры_и_субтитры/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. 
        // Дополнительные сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_ТИС,Язык_текста,Продолжительность_титров")] Титры_и_субтитры титры_и_субтитры)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Entry(титры_и_субтитры).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                return View(титры_и_субтитры);
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

        }

        // GET: Титры_и_субтитры/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Титры_и_субтитры титры_и_субтитры = db.Титры_и_субтитры.Find(id);
            if (титры_и_субтитры == null)
            {
                return HttpNotFound();
            }
            return View(титры_и_субтитры);
        }

        // POST: Титры_и_субтитры/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                Титры_и_субтитры титры_и_субтитры = db.Титры_и_субтитры.Find(id);
                db.Титры_и_субтитры.Remove(титры_и_субтитры);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
