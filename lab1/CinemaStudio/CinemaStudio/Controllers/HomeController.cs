﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CinemaStudio.Models;

namespace CarRentCompany.Controllers
{
    public class HomeController : Controller
    {
        private CinemaStudioModel db = new CinemaStudioModel();

        public ActionResult Index()
        {
            return View();
        }

        #region Вход \ Выход

        public ActionResult SignInPage()
        {
            return View();
        }

        public ActionResult SignOut()
        {
            Response.Cookies["RoleCookie"].Expires = DateTime.Now.AddDays(-1d);

            return RedirectToAction("Index");
        }

        #endregion

        #region  Сохранение cookie для каждого пользователя.

        public ActionResult Client()
        {
            HttpCookie clientcookie = new HttpCookie("RoleCookie");
            clientcookie.Value = "Client";
            Response.Cookies.Add(clientcookie);

            return RedirectToAction("HomeClientPage");
        }

        public ActionResult Employee()
        {
            HttpCookie employeecookie = new HttpCookie("RoleCookie");
            employeecookie.Value = "Employee";
            Response.Cookies.Add(employeecookie);

            return RedirectToAction("HomeEmployeePage");
        }

        public ActionResult Administrator()
        {
            HttpCookie admincookie = new HttpCookie("RoleCookie");
            admincookie.Value = "Admin";
            Response.Cookies.Add(admincookie);

            return RedirectToAction("HomeAdminPage");
        }

        #endregion

        #region Личный кабинет каждого пользователя

        public ActionResult HomeClientPage(Человек model)
        {
            return View(model);
        }

        public ActionResult HomeEmployeePage()
        {
            return View();
        }

        public ActionResult HomeAdminPage()
        {
            return View();
        }

        #endregion
    }
}