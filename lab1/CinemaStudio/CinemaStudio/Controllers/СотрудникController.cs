﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CinemaStudio.Models;

namespace CinemaStudio.Controllers
{
    public class СотрудникController : Controller
    {
        private CinemaStudioModel db = new CinemaStudioModel();

        // GET: Сотрудник
        public ActionResult Index()
        {
            var сотрудник = db.Сотрудник.Include(с => с.Человек);
            return View(сотрудник.ToList());
        }


        [HttpPost]
        public ActionResult Index(string post, int? pay)
        {
            if (post == "" || pay == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var Сотрудник = db.Сотрудник
                .Where(a => a.Должность == post)
                .Where(a => a.ЗП == pay);
            return View(Сотрудник.ToList());
        }

        // GET: Сотрудник/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Сотрудник сотрудник = db.Сотрудник.Find(id);
            if (сотрудник == null)
            {
                return HttpNotFound();
            }
            return View(сотрудник);
        }

        // GET: Сотрудник/Create
        public ActionResult Create()
        {
            ViewBag.ID_Человека = new SelectList(db.Человек, "ID_Человека", "Имя");
            return View();
        }

        // POST: Сотрудник/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. 
        // Дополнительные сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID_Сотрудника,Должность,ЗП,СтажРаботы,ID_Человека")] Сотрудник сотрудник)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Сотрудник.Add(сотрудник);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }

                ViewBag.ID_Человека = new SelectList(db.Человек, "ID_Человека", "Имя", сотрудник.ID_Человека);
                return View(сотрудник);
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

        }

        // GET: Сотрудник/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Сотрудник сотрудник = db.Сотрудник.Find(id);
            if (сотрудник == null)
            {
                return HttpNotFound();
            }
            ViewBag.ID_Человека = new SelectList(db.Человек, "ID_Человека", "Имя", сотрудник.ID_Человека);
            return View(сотрудник);
        }

        // POST: Сотрудник/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. 
        // Дополнительные сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_Сотрудника,Должность,ЗП,СтажРаботы,ID_Человека")] Сотрудник сотрудник)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Entry(сотрудник).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                ViewBag.ID_Человека = new SelectList(db.Человек, "ID_Человека", "Имя", сотрудник.ID_Человека);
                return View(сотрудник);
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

        }

        // GET: Сотрудник/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Сотрудник сотрудник = db.Сотрудник.Find(id);
            if (сотрудник == null)
            {
                return HttpNotFound();
            }
            return View(сотрудник);
        }

        // POST: Сотрудник/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                Сотрудник сотрудник = db.Сотрудник.Find(id);
                db.Сотрудник.Remove(сотрудник);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
