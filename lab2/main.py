import random

import matplotlib.pyplot as plt
import numpy as np


k = int(input("K = "))
n = int(input("N = "))

if n % 2:
    raise ValueError("N must be even")

a = np.fromiter((random.randint(-10, 10) for _ in range(n * n)), dtype=np.int32).reshape(n, n)

half_n = n // 2
b = a[:half_n, :half_n]
c = a[:half_n, half_n:]
d = a[half_n:, :half_n]
e = a[half_n:, half_n:]

f = a.copy()

print(f, b, c, d, e, sep="\n\n", end="\n\n")

if np.sum(b[:, 1::2] == 0) > np.sum(b[::2, :]):
    f[:half_n, :] = f[:half_n, ::-1]
else:
    f[half_n:, half_n:], f[:half_n, :half_n] = b.copy(), e.copy()

print("После обмена", f, sep="\n\n", end="\n\n")

if np.linalg.det(a) > (np.trace(f) + np.trace(np.fliplr(f))):
    result = np.linalg.inv(f) * np.transpose(a) - k * np.transpose(f)
else:
    result = (np.transpose(a) + np.tril(a) - np.linalg.inv(f)) * k

print("Результат", result, sep="\n\n", end="\n\n")

plt.subplot(2, 2, 1)
plt.imshow(f[:half_n, :half_n], cmap="rainbow", interpolation="bilinear")
plt.subplot(2, 2, 2)
plt.imshow(f[:half_n, half_n:], cmap="rainbow", interpolation="bilinear")
plt.subplot(2, 2, 3)
plt.imshow(f[half_n:, :half_n], cmap="rainbow", interpolation="bilinear")
plt.subplot(2, 2, 4)
plt.imshow(f[half_n:, half_n:], cmap="rainbow", interpolation="bilinear")
plt.show()

plt.subplot(2, 2, 1)
plt.plot(f[:half_n, :half_n])
plt.subplot(2, 2, 2)
plt.plot(f[:half_n, half_n:])
plt.subplot(2, 2, 3)
plt.plot(f[half_n:, :half_n])
plt.subplot(2, 2, 4)
plt.plot(f[half_n:, half_n:])
plt.show()
