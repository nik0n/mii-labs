import matplotlib.pyplot as pyplot
import pandas
import pylab
from sklearn.neighbors import KNeighborsClassifier
from sklearn.preprocessing import StandardScaler
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier


df = pandas.read_csv("mall_customers.csv")
teach_df = df.iloc[:150]
test_df = df.iloc[50:]


def init(df):
    df = df.copy()
    df = df.drop("CustomerID", axis=1)
    df['Genre'] = pandas.factorize(df['Genre'])[0]
    X = df.drop("Spending Score", axis=1)
    y = df["Spending Score"]
    X = pandas.DataFrame(X, index=X.index, columns=X.columns)
    return X, y


scaler = StandardScaler()
X_teach, y_teach = init(teach_df)
X_teach = scaler.fit_transform(X_teach)
X_test, y_test = init(test_df)
X_test = scaler.fit_transform(X_test)

knn = KNeighborsClassifier().fit(X_teach, y_teach)
knn_predictions = pandas.Series(knn.predict(X_test))
print("Точность предсказаний KNN: " + str(knn.score(X_test, y_test) * 100) + "%")

svm = SVC(kernel="rbf").fit(X_teach, y_teach)
svm_predictions = pandas.Series(svm.predict(X_test))
print("Точность предсказаний SVM: " + str(svm.score(X_test, y_test) * 100) + "%")

dtc = DecisionTreeClassifier().fit(X_teach, y_teach)
dtc_predictions = pandas.Series(dtc.predict(X_test))
print("Точность предсказаний DTC: " + str(dtc.score(X_test, y_test) * 100) + "%")

pylab.figure(figsize=(20, 10))
pylab.subplot(1, 2, 1)
pyplot.pie(y_test.value_counts().sort_index(), labels=sorted(y_test.unique()), autopct="%1.1f%%")
pyplot.title("Текущая оценка расходов")
pylab.subplot(1, 2, 2)
pyplot.pie(knn_predictions.value_counts().sort_index(), labels=sorted(knn_predictions.unique()), autopct="%1.1f%%")
pyplot.title("Оценка расходов по KNN")
pyplot.show()

pylab.figure(figsize=(20, 10))
pylab.subplot(1, 2, 1)
pyplot.pie(y_test.value_counts().sort_index(), labels=sorted(y_test.unique()), autopct="%1.1f%%")
pyplot.title("Текущая оценка расходов")
pylab.subplot(1, 2, 2)
pyplot.pie(dtc_predictions.value_counts().sort_index(), labels=sorted(dtc_predictions.unique()), autopct="%1.1f%%")
pyplot.title("Оценка расходов по DTC")
pyplot.show()

pylab.figure(figsize=(20, 10))
pylab.subplot(1, 2, 1)
pyplot.pie(y_test.value_counts().sort_index(), labels=sorted(y_test.unique()), autopct="%1.1f%%")
pyplot.title("Текущая оценка расходов")
pylab.subplot(1, 2, 2)
pyplot.pie(svm_predictions.value_counts().sort_index(), labels=sorted(svm_predictions.unique()), autopct="%1.1f%%")
pyplot.title("Оценка расходов по SVN")
pyplot.show()
