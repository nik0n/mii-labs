from collections import Counter
from csv import reader, writer
from datetime import date
from pathlib import Path
from random import choice, randint

import matplotlib.dates as mdates
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from mimesis import Code, Person
from mimesis.enums import Gender, Locale

from consts import *
from data_source import *


person = Person(Locale.RU)
code = Code()


def generate_data(rows: int):
    current_year = date.today().year
    data = []
    for _ in range(rows):
        gender = choice([Gender.MALE, Gender.FEMALE])
        birth_year = current_year - randint(PERSON_AGE_FROM, PERSON_AGE_TO)
        department = choice(list(COMPANY_STRUCTURE))

        data.append(
            [
                person.identifier(),
                f"{person.surname(gender)} {person.name(gender)[0]}. {person.name(Gender.MALE)[0]}.",
                gender.value,
                birth_year,
                min(current_year, birth_year + randint(EMPLOYMENT_AGE_FROM, EMPLOYMENT_AGE_TO)),
                department,
                choice(COMPANY_STRUCTURE[department]),
                randint(SALARY_FROM, SALARY_TO),
                randint(COMPLETED_PROJECTS_FROM, COMPLETED_PROJECTS_TO),
            ]
        )

    return data


def np_analyze():
    with open(CSV_FILE_PATH, encoding="utf-8") as csv_file:
        data_raw = [list(row) for row in reader(csv_file)]

    data = np.array(data_raw)
    genders = data[:, 2]
    salary = data[:, 7].astype("float64")
    projects = data[:, 8].astype("int32")

    print("NumPy")
    print("% женщин:", np.sum(genders == "female") / np.size(genders))
    print("Средняя з/п:", np.average(salary))
    print("Минимальная з/п:", np.min(salary))
    print("Максимальная з/п:", np.max(salary))
    print("Медиана количества проектов:", np.median(projects))
    print("Среднеквадратическое отклонение кол-ва проектов:", np.std(projects))
    print("Мода количества проектов:", Counter(projects).most_common(1)[0][0])


def pandas_analyze():
    data = pd.read_csv(CSV_FILE_PATH, header=None)
    genders = data[2]
    salary = data[7]
    projects = data[8]
    print("Pandas")
    print("% женщин:", genders.value_counts()["female"] / genders.shape[0])
    print("Средняя з/п:", salary.mean())
    print("Минимальная з/п:", salary.min())
    print("Максимальная з/п:", salary.max())
    print("Медиана количества проектов:", projects.median())
    print("Среднеквадратическое отклонение количества проектов:", projects.std())
    print("Мода количества проектов:", projects.mode()[0])


    plt.figure(figsize=(14, 10), dpi=80)
    plt.hlines(y=projects, xmin=0, xmax=salary, color="C0", alpha=0.4, linewidth=5)
    plt.gca().set(ylabel="Число проектов", xlabel="з/п")
    plt.show()

    plt.pie([genders.value_counts()["male"], genders.value_counts()["female"]], labels=["Мужчины", "Женщины"])
    plt.show()

    plt.figure(figsize=(16, 10), dpi=80)
    plt.plot_date(data[4], salary)
    plt.gca().xaxis.set_major_locator(mdates.AutoDateLocator())
    plt.ylabel("з/п")
    plt.xlabel("Даты")
    plt.show()


def main():
    with open(CSV_FILE_PATH, "w", encoding="utf-8") as csv_file:
        writer(csv_file, lineterminator="\n").writerows(generate_data(randint(ROW_COUNT_FROM, ROW_COUNT_TO)))

    np_analyze()
    print("\n==========================\n")
    pandas_analyze()


if __name__ == "__main__":
    main()
